package com.scvh.dedsanya.controllers;

import com.scvh.dedsanya.player.SanyaState;
import com.scvh.dedsanya.player.Score;

/**
 * Handling score, yes it's done by sleep method and i know that it's weird
 */
public class ScoreHandler extends Thread {

    private Score score;
    private SanyaState sanyaState;

    /**
     * Instantiates a new Score handler.
     *
     * @param score      the score
     * @param sanyaState the sanya state
     */
    public ScoreHandler(Score score, SanyaState sanyaState) {
        this.score = score;
        this.sanyaState = sanyaState;
    }

    @Override
    public void run() {
        while (sanyaState == SanyaState.Running) {
            score.increaseScore();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
