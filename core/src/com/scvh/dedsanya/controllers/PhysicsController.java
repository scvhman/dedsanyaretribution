package com.scvh.dedsanya.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.scvh.dedsanya.World.GameWorld;
import com.scvh.dedsanya.player.Sanya;
import com.scvh.dedsanya.player.SanyaState;

/**
 * Physics controller.
 */
public class PhysicsController {

    private World physicsWorld;
    private GameWorld gameWorld;
    private Body sanyaPhysicsBody;
    private Sanya sanya;

    /**
     * Instantiates a new Physics controller.
     *
     * @param gravity   the gravity
     * @param gameWorld the game world
     */
    public PhysicsController(Vector2 gravity, GameWorld gameWorld) {
        physicsWorld = new World(gravity, true);
        this.gameWorld = gameWorld;
        sanya = gameWorld.getSanya();
        sanyaInitialize();
    }

    private void sanyaInitialize() {
        BodyDef sanyaDefinition = new BodyDef();
        sanyaDefinition.type = BodyDef.BodyType.DynamicBody;
        sanyaDefinition.position.set(sanya.getSanyaPosition().x, sanya.getSanyaPosition().y);
        sanyaPhysicsBody = physicsWorld.createBody(sanyaDefinition);
        PolygonShape sanyaBodyShape = new PolygonShape();
        sanyaBodyShape.setAsBox(sanya.getSanyaSprite().getWidth(), sanya.getSanyaSprite().getHeight());
        sanyaPhysicsBody.setLinearVelocity(new Vector2(2f, 0));
        FixtureDef sanyaFixture = new FixtureDef();
        sanyaFixture.density = 1f;
        sanyaFixture.shape = sanyaBodyShape;
        sanyaPhysicsBody.createFixture(sanyaFixture);
        sanyaBodyShape.dispose();
    }

    /**
     * Make sanya jump
     */
    public void jumpSanya() {
        if (sanya.getStateOfTheHero() == SanyaState.Running) {
            sanya.jump(sanyaPhysicsBody, new Vector2(0, 10f));
        }
    }

    /**
     * Render physics.
     */
    public void renderPhysics() {
        sanya.setSanyaPosition(new Vector2(sanyaPhysicsBody.getPosition().x * 100, sanyaPhysicsBody.getPosition().y * 100));
        physicsWorld.step(Gdx.graphics.getDeltaTime(), 6, 2);
    }
}
