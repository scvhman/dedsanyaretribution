package com.scvh.dedsanya.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.scvh.dedsanya.Enemies.EnemyController;
import com.scvh.dedsanya.Enemies.EnemySpawner;
import com.scvh.dedsanya.World.GameWorld;

/**
 * Class controlling game world
 */
public class WorldController {

    private GameWorld gameWorld;
    private ScoreHandler scoreInreaser;
    private PhysicsController physics;
    private InputHandler input;
    private EnemySpawner enemies;

    /**
     * Instantiates a new World controller.
     *
     * @param gameWorld the game world
     */
    public WorldController(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        scoreInreaser = new ScoreHandler(gameWorld.getSanya().getScore(), gameWorld.getSanya().getStateOfTheHero());
        physics = new PhysicsController(new Vector2(0, 0), gameWorld);
        input = new InputHandler(physics);
        EnemyController enemyController = new EnemyController(gameWorld);
        enemies = new EnemySpawner(enemyController);
    }

    /**
     * Start controlling.
     */
    public void startControlling() {
        scoreInreaser.start();
//        enemies.start();
        Gdx.input.setInputProcessor(input);
    }

    /**
     * Handle physics.
     */
    public void handlePhysics() {
        physics.renderPhysics();
    }
}
