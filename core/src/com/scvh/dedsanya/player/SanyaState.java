package com.scvh.dedsanya.player;

/**
 * Sanya state. He can be either caught or running
 */
public enum SanyaState {

    /**
     * Caught sanya state.
     */
    Caught, /**
     * Running sanya state.
     */
    Running

}
