package com.scvh.dedsanya.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

/**
 * Sanyok as is
 */
public class Sanya {

    private Score score;
    private SanyaState stateOfTheHero;
    private Vector2 sanyaPosition;
    private Sprite sanyaSprite;

    /**
     * Instantiates a new Sanya.
     */
    public Sanya() {
        Texture sanyaTexture = new Texture(Gdx.files.internal("sanya.png"));
        score = new Score(0);
        stateOfTheHero = SanyaState.Running;
        sanyaPosition = new Vector2(1, 1);
        sanyaSprite = new Sprite(sanyaTexture);
    }

    /**
     * Gets sanya sprite.
     *
     * @return the sanya sprite
     */
    public Sprite getSanyaSprite() {
        return sanyaSprite;
    }

    /**
     * Gets score.
     *
     * @return the score
     */
    public Score getScore() {
        return score;
    }

    /**
     * Gets state of the hero.
     *
     * @return the state of the hero
     */
    public SanyaState getStateOfTheHero() {
        return stateOfTheHero;
    }

    /**
     * Gets sanya position.
     *
     * @return the sanya position
     */
    public Vector2 getSanyaPosition() {
        return sanyaPosition;
    }

    /**
     * Make sanya jumping
     *
     * @param body  the body
     * @param force the force
     */
    public void jump(Body body, Vector2 force) {
        body.applyForceToCenter(force, true);
    }

    /**
     * Sets sanya position.
     *
     * @param sanyaPosition the sanya position
     */
    public void setSanyaPosition(Vector2 sanyaPosition) {
        this.sanyaPosition = sanyaPosition;
    }
}
