package com.scvh.dedsanya.player;

/**
 * Score
 */
public class Score {

    private long score;

    /**
     * Instantiates a new Score.
     *
     * @param score the score
     */
    public Score(long score) {
        this.score = score;
    }

    /**
     * Increase score.
     */
    public void increaseScore() {
        score = score + 1;
    }

    /**
     * Gets score.
     *
     * @return the score
     */
    public long getScore() {
        return score;
    }
}
