package com.scvh.dedsanya;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.scvh.dedsanya.Rendering.WorldRenderer;
import com.scvh.dedsanya.World.GameWorld;
import com.scvh.dedsanya.controllers.WorldController;

/**
 * The main class
 */
public class SanyaGame extends ApplicationAdapter {

    private GameWorld gameWorld;
    private WorldController worldController;
    private WorldRenderer worldRenderer;

    @Override
    public void create() {
        gameWorld = new GameWorld();
		worldController = new WorldController(gameWorld);
        worldRenderer = new WorldRenderer(gameWorld);
        worldController.startControlling();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        worldRenderer.render();
        worldController.handlePhysics();
    }

    @Override
    public void dispose() {

    }
}
