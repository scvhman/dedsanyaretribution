package com.scvh.dedsanya.Rendering;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scvh.dedsanya.Enemies.Enemy;
import com.scvh.dedsanya.World.Background;
import com.scvh.dedsanya.World.GameWorld;
import com.scvh.dedsanya.player.Sanya;

import java.util.Iterator;

/**
 * World renderer.
 */
public class WorldRenderer {

    private GameWorld gameWorld;
    private SpriteBatch renderingBatch;
    private Background background;
    private Background scroller;
    private Sanya sanya;
    private BitmapFont scoreFont;
    private String score;
    private OrthographicCamera camera;
    private Iterator<Enemy> enemiesInWorld;
    private Enemy iterableEnemy;
    private float backgroundX, scrollerX;

    /**
     * Instantiates a new World renderer.
     *
     * @param gameWorld the game world
     */
    public WorldRenderer(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        renderingBatch = new SpriteBatch();
        background = gameWorld.getBackground();
        scroller = gameWorld.getScroller();
        sanya = gameWorld.getSanya();
        scoreFont = new BitmapFont();
        scoreFont.getData().scale(2);
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        backgroundX = background.getPosition().x;
        scrollerX = scroller.getPosition().x;
        enemiesInWorld = gameWorld.getEnemies().iterator();
    }

    /**
     * Render world
     */
    public void render() {
        camera.position.set(gameWorld.getSanya().getSanyaPosition().x + 720, gameWorld.getSanya().getSanyaPosition().y * 6, 0);
        camera.update();
        renderingBatch.setProjectionMatrix(camera.combined);
        renderBackground();
        renderCrazySanya();
        renderEnemies();
        renderFont();
    }

    private void renderCrazySanya() {
        renderingBatch.begin();
        renderingBatch.draw(sanya.getSanyaSprite(), gameWorld.getSanya().getSanyaPosition().x, gameWorld.getSanya().getSanyaPosition().y,
                Gdx.graphics.getHeight() / 4, Gdx.graphics.getWidth() / 6);
        renderingBatch.end();
    }

    private void renderBackground() {
        scrollerX = backgroundX + background.getTexture().getWidth();
        if (camera.position.x >= scrollerX + camera.viewportWidth / 2) {
            backgroundX = scrollerX;
        }
        renderingBatch.begin();
        renderingBatch.draw(background.getTexture(), backgroundX, background.getPosition().y);
        renderingBatch.draw(scroller.getTexture(), scrollerX, background.getPosition().y);
        renderingBatch.end();
    }

    private void renderFont() {
        renderingBatch.begin();
        score = "Score:" + String.valueOf(gameWorld.getSanya().getScore().getScore());
        scoreFont.draw(renderingBatch, score, gameWorld.getSanya().getSanyaPosition().x, Gdx.graphics.getHeight());
        renderingBatch.end();
    }

    private void renderEnemies() {
        while (enemiesInWorld.hasNext()) {
            renderingBatch.begin();
            iterableEnemy = enemiesInWorld.next();
            renderingBatch.draw(iterableEnemy.getEnemySprite(), iterableEnemy.getEnemyPosition().x, iterableEnemy.getEnemyPosition().y);
            renderingBatch.end();
        }
    }
}
