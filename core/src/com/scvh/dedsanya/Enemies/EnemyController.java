package com.scvh.dedsanya.Enemies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.scvh.dedsanya.World.GameWorld;

import java.util.ArrayList;

/**
 * Class controlling enemies in gameworld
 */
public class EnemyController {

    private GameWorld gameWorld;
    private Enemy dekan;


    /**
     * Instantiates a new Enemy controller.
     *
     * @param gameWorld the game world
     */
    public EnemyController(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        initEnemies();
    }

    /**
     * Everything done here is only for test purpose
     *
     * @author scvhabodka-man
     * @version 1.0
     */
    private void initEnemies() {
        Texture dekanTexture = new Texture(Gdx.files.internal("sanya.png"));
        dekan = new Enemy(dekanTexture);
        dekan.setEnemyPosition(new Vector2(200, 200));
        gameWorld.addEnemy(dekan);
    }


    /**
     * Gets enemies.
     *
     * @return the enemies
     */
    public ArrayList<Enemy> getEnemies() {
        return gameWorld.getEnemies();
    }


    /**
     * Gets game world.
     *
     * @return the game world
     */
    public GameWorld getGameWorld() {
        return gameWorld;
    }
}
