package com.scvh.dedsanya.Enemies;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Enemy {

    private Sprite enemySprite;
    private Vector2 enemyPosition;

    public Enemy(Texture texture) {
        enemySprite = new Sprite(texture);
    }

    public Sprite getEnemySprite() {
        return enemySprite;
    }

    public Vector2 getEnemyPosition() {
        return enemyPosition;
    }

    public void setEnemyPosition(Vector2 enemyPosition) {
        this.enemyPosition = enemyPosition;
    }
}
