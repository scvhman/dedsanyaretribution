package com.scvh.dedsanya.Enemies;

import com.scvh.dedsanya.player.SanyaState;

/**
 * Spawns enemies
 */
public class EnemySpawner extends Thread {

    private EnemyController controller;

    /**
     * Instantiates a new Enemy spawner.
     *
     * @param controller the controller
     */
    public EnemySpawner(EnemyController controller) {
        this.controller = controller;
    }

    @Override
    public void run() {
        while (controller.getGameWorld().getSanya().getStateOfTheHero() == SanyaState.Running) {
            Enemy enemy = controller.getEnemies().get(0);
            enemy.setEnemyPosition(controller.getGameWorld().getSanya().getSanyaPosition().add(50, 0));
        }
    }
}
