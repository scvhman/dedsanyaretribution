package com.scvh.dedsanya.World;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * It's basically world background
 */
public class Background {

    private Sprite texture;
    private int speed;
    private Vector2 position;

    /**
     * Instantiates a new Background.
     *
     * @param position the position
     */
    public Background(Vector2 position) {
        this.position = position;
        Texture worldTexture = new Texture(Gdx.files.internal("bg.png"));
        texture = new Sprite(worldTexture);
        texture.setPosition(position.x, position.y);
        speed = 0;
    }

    /**
     * Gets position of bg in (x, y)
     *
     * @return the position
     */
    public Vector2 getPosition() {
        return position;
    }

    /**
     * Sets position.
     *
     * @param position the position
     */
    public void setPosition(Vector2 position) {
        this.position = position;
        texture.setPosition(position.x, position.y);
    }

    /**
     * Gets texture.
     *
     * @return the texture
     */
    public Sprite getTexture() {
        return texture;
    }

}
