package com.scvh.dedsanya.World;

import com.badlogic.gdx.math.Vector2;
import com.scvh.dedsanya.Enemies.Enemy;
import com.scvh.dedsanya.player.Sanya;

import java.util.ArrayList;

/**
 * Game world
 */
public class GameWorld {

    private Sanya sanya;
    private Background background;
    private Background scroller; // this is secong bg using for infinite looping
    private ArrayList<Enemy> enemies;

    /**
     * Instantiates a new Game world.
     */
    public GameWorld() {
        background = new Background(new Vector2(0, 0));
        scroller = new Background(new Vector2(0, 0));
        sanya = new Sanya();
        enemies = new ArrayList<Enemy>();
    }

    /**
     * Gets background.
     *
     * @return the background
     */
    public Background getBackground() {
        return background;
    }

    /**
     * Gets scroller(it's basically second bg for scrolling)
     *
     * @return the scroller
     */
    public Background getScroller() {
        return scroller;
    }

    /**
     * Gets sanya.
     *
     * @return the sanya
     */
    public Sanya getSanya() {
        return sanya;
    }

    /**
     * Add enemy.
     *
     * @param enemy the enemy
     */
    public void addEnemy(Enemy enemy) {
        enemies.add(enemy);
    }

    /**
     * Gets enemies.
     *
     * @return the enemies
     */
    public ArrayList<Enemy> getEnemies() {
        return enemies;
    }
}
